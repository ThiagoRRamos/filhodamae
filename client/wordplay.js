////////// Main client application logic //////////

var player = function () {
  return Meteor.user();
};

var game = function () {
  var me = player();
  return me && me.game_id && Games.findOne(me.game_id);
};

var thisround = function(){
	return game() && game().last_round && Rounds.findOne({_id:game().last_round})
};

var thisminiround = function(){
	return thisround() && thisround().last_miniround && MiniRounds.findOne(thisround().last_miniround)
};

var myturntoplay = function(){
	return thisminiround() && thisminiround().number_play==thisminiround().player_order.indexOf(player()._id)
};

var myturntopredict = function(){
	return thisround() && thisround().number_pred==thisround().player_order.indexOf(player()._id)
};

Template.lobby.show = function () {
  return !game();
};

Template.lobby.waiting = function () {
  if(player()){
    var players = Meteor.users.find({_id: {$ne: player()._id},
                              game_id: {$exists: false}});

    return players.fetch();
  }else{
    var players = Meteor.users.find({game_id: {$exists: false}});

    return players.fetch();
  }
};

Template.lobby.count = function () {
  if(player()){
    var players = Meteor.users.find({_id: {$ne: player()._id},
                              game_id: {$exists: false}});
    return players.count();
  }else{
    var players = Meteor.users.find({game_id: {$exists: false}});
    return players.count();
  }
  
};

Template.lobby.disabled = function () {
  var me = player();
  if (me && me.profile && me.profile.name)
    return '';
  return 'disabled="disabled"';
};


Template.lobby.events({
  'click button.startgame': function () {
	console.log(Meteor.users.find().fetch())
    	game_id = Meteor.call('start_new_game',function(error,result){
		Session.set('game_id',result);
		Meteor.call('start_new_round',Session.get('game_id'));
	});
  }
});

Template.scores.show = function () {
  return !!game();
};

Template.scores.players = function () {
  return game() && game().players;
};

Template.minhascartas.mycards = function(){
	return player() && player().current_hand;
}

Template.minhascartas.show = function(){
	return game()
}

var renderizar_naipe = function(na){
	switch(na){
		case 'C': return new Handlebars.SafeString('&hearts;');
		case 'P': return new Handlebars.SafeString('&clubs;');
		case 'E': return new Handlebars.SafeString('&spades;');
		case 'O': return new Handlebars.SafeString('&diams;');
	}
}

Template.minhascartas.naipe = renderizar_naipe

var imagem = function(naipe,number){
	vv = number
	switch(number){
		case 'A': vv = 'ace';break;
		case 'J': vv = 'jack';break;
		case 'Q': vv = 'queen';break;
		case 'K': vv = 'king';break;
	}
	switch(naipe){
		case "O": return "/cards/"+vv+"_of_diamonds.svg"	
		case "E": return "/cards/"+vv+"_of_spades.svg"
		case "C": return "/cards/"+vv+"_of_hearts.svg"
		case "P": return "/cards/"+vv+"_of_clubs.svg"
	}	
}

Template.minhascartas.imagem = imagem

Template.cartasjogadas.imagem = imagem

Template.minhascartas.events = {
	"click .minha-carta" : function(){
		if(myturntoplay()){
			Meteor.call('send_card',thisminiround()._id,this)
		}
	}
}

Template.myprediction.show = function(){
	return myturntopredict();
}

Template.predictions.show = function(){
	return !!game();
}

Template.scores.players = function(){
	return game() && game().players;
}

Template.predictions.predictions = function(){
	return thisround() && thisround().predictions;
}

var acusarerrodepredicao = function(error,result){
	if(!result){
		$("#predresultado").text("Nao possivel")
	}
}

Template.myprediction.events({
	"click button" : function(){
				if(myturntopredict()){
					valor = $('#myprediction').val()
					if(!isNaN(valor)){
						Meteor.call('send_prediction',game().last_round,valor, acusarerrodepredicao)	
					}
					
				}
			}
});

Template.avisos.vezdejogar = myturntoplay
Template.avisos.vezdeprever = myturntopredict

Template.cartasjogadas.playedcards = function(){
	if(!thisround())
		return false
	array = []
	game_id = game()._id
	round_id = thisround()._id
	i = 0
	MiniRounds.find({game_id:game_id,round_id:round_id}).forEach(function(dat){
		i++;
		array.push({number:i,plays:dat.plays})
	})
	return array
}

Template.cartasjogadas.naipe = renderizar_naipe

Template.cartasjogadas.show = function(){
	return game()
}

Template.vitorias.vits = function(){
	return thisround() && thisround().minirounds
}

Template.vitorias.show = function(){
	return game()
}

Template.conversa.show = function(){
	return game()
}

Template.conversa.frases = function(){
	return game().conversa
}

Template.conversa.events({
	"click button": function(){
		mensagem = $('#minha-fala').val()
		Games.update({_id:game()._id},{$push:{conversa:{_id:player()._id,name:player().profile.name,mensagem:mensagem}}})
		$('#minha-fala').val("")
	}
})

var estar_vivo = function(){
	if(player()){
		now = new Date()
		Meteor.users.update({_id:player()._id},{$set:{'profile.last_p':now.getTime(),'profile.idle':false}})
	}
}

//////
////// Initialization
//////

Meteor.startup(function () {

  Meteor.autosubscribe(function () {
    Meteor.subscribe('userData')
    Meteor.subscribe('other-users')
    if (player() && player().game_id) {
        Meteor.subscribe('games', player().game_id);
	Meteor.subscribe('rounds',player().game_id)
	Meteor.subscribe('minirounds',player().game_id)
    }
  });
});

Meteor.setInterval(estar_vivo,1000*60*20);
