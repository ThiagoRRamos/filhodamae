////////// Server only logic //////////

function generateNCards(n){
	var numbers = generateNNumbers(n,40)
	var lista = []
	for(elem in numbers){
		lista.push(gerarCarta(numbers[elem]))
	}
	return lista
}

function generateNNumbers(n,limit){
	var arr = []
	while(arr.length < n){
  		var randomnumber=Math.ceil(Math.random()*limit)
  		var found=false;
  		for(var i=0;i<arr.length;i++){
    			if(arr[i]==randomnumber){found=true;break}
  		}
  		if(!found)arr[arr.length]=randomnumber;
	}
	return arr;
}

function gerarCarta(elem){
	//Change to Array
	//Nao fazer agora porque esta funcionando. Mudar quando nao tiver o que fazer
	switch(elem){
		case 1: return {na:'O', nu:'4', value:1}
		case 2:	return {na:'E', nu:'4', value:2}
		case 3:	return {na:'C', nu:'4', value:3}
		case 4:	return {na:'O', nu:'5', value:4}
		case 5: return {na:'E', nu:'5', value:5}
		case 6: return {na:'C', nu:'5', value:6}
		case 7: return {na:'P', nu:'5', value:7}
		case 8: return {na:'O', nu:'6', value:8}
		case 9: return {na:'E', nu:'6', value:9}
		case 10: return {na:'C', nu:'6', value:10}
		case 11: return {na:'P', nu:'6', value:11}
		case 12: return {na:'E', nu:'7', value:12}
		case 13: return {na:'P', nu:'7', value:13}
		case 14: return {na:'O', nu:'Q', value:14}
		case 15: return {na:'E', nu:'Q', value:15}
		case 16: return {na:'C', nu:'Q', value:16}
		case 17: return {na:'P', nu:'Q', value:17}
		case 18: return {na:'O', nu:'J', value:18}
		case 19: return {na:'E', nu:'J', value:19}
		case 20: return {na:'C', nu:'J', value:20}
		case 21: return {na:'P', nu:'J', value:21}
		case 22: return {na:'O', nu:'K', value:22}
		case 23: return {na:'E', nu:'K', value:23}
		case 24: return {na:'C', nu:'K', value:24}
		case 25: return {na:'P', nu:'K', value:25}
		case 26: return {na:'O', nu:'A', value:26}
		case 27: return {na:'C', nu:'A', value:27}
		case 28: return {na:'P', nu:'A', value:28}
		case 29: return {na:'O', nu:'2', value:29}
		case 30: return {na:'E', nu:'2', value:30}
		case 31: return {na:'C', nu:'2', value:31}
		case 32: return {na:'P', nu:'2', value:32}
		case 33: return {na:'O', nu:'3', value:33}
		case 34: return {na:'E', nu:'3', value:34}
		case 35: return {na:'C', nu:'3', value:35}
		case 36: return {na:'P', nu:'3', value:36}
		case 37: return {na:'O', nu:'7', value:37}
		case 38: return {na:'E', nu:'A', value:38}
		case 39: return {na:'C', nu:'7', value:39}
		case 40: return {na:'P', nu:'4', value:40}
	}
}

var start_game = function (evt) {
    	var game_id = Games.insert({});
    	Meteor.users.update({game_id: null},
	           {$set: {game_id: game_id}},
	           {multi: true});
	var lista = []
	var pessoas = []
    	var ps = Meteor.users.find({game_id: game_id},
	                 {fields: {_id: true,profile: true}}).forEach(function (post){
									lista.push(post._id)
									pessoas.push({id:post._id,name:post.profile.name,points:5})
									})
    	Games.update({_id: game_id}, {$set: {player_order: lista,players : pessoas}});
    	return game_id;
}

var start_round = function(game) {
	var round_id = Rounds.insert({game_id : game})
	current_game = Games.findOne({_id:game})
	if(current_game.last_round){
		last_round = Rounds.findOne({_id:current_game.last_round})
		player_order = last_round.player_order
		first_player = player_order.shift()
		player_order.push(first_player)
		elimins = last_round.eliminados
		for(el in elimins){
			ind = player_order.indexOf(elimins[el])
			if(ind!=-1){
				player_order.splice(ind,1)
			}
		}
		number_of_cards = last_round.n_cards+1
		if(number_of_cards*player_order.length>40){
			number_of_cards = number_of_cards-1;
		}
	}else{
		player_order = current_game.player_order
		number_of_cards = 1
	}
	cards = generateNCards(number_of_cards*player_order.length)
	count = 0
	for(player in player_order){
		Meteor.users.update({_id:player_order[player]},{$set:{current_hand:cards.slice(number_of_cards*count,number_of_cards*(count+1))}})
		count = count+1
	}
	Rounds.update({_id:round_id},{$set: {player_order : player_order,n_cards : number_of_cards,number_pred:0}})
	Games.update({_id:game},{$push:{rounds : round_id},$set:{last_round:round_id}})
	return round_id
}

var start_miniround = function(round_id){
	game_id = Rounds.findOne(round_id).game_id
	miniround_id = MiniRounds.insert({game_id : game_id,round_id : round_id,number_play : 0})
	current_round = Rounds.findOne(round_id)
	if(current_round.last_miniround){
		last_miniround = MiniRounds.findOne(current_round.last_miniround)
		player_order = last_miniround.player_order
		ind = player_order.indexOf(last_miniround.winner.id)
		player_order = player_order.slice(ind,player_order.length).concat(player_order.slice(0,ind))
	}else{
		player_order = current_round.player_order
	}
	plays = []
	for(pp in current_round.player_order){
		data = {}
		username = Meteor.users.findOne(current_round.player_order[pp]).profile.name
		data.id = current_round.player_order[pp]
		data.name = username
		plays.push(data)
	}
	MiniRounds.update({_id:miniround_id},{$set:{player_order:player_order,plays : plays}})
	Rounds.update({_id:round_id},{$set:{last_miniround : miniround_id},$push:{minirounds : {miniround:miniround_id}}})
	return miniround_id
}

var predictions_are_over = function(round_id){
	tround = Rounds.findOne(round_id)
	return tround.number_pred==tround.player_order.length
}

var miniround_is_over = function(miniround_id){
	miniround = MiniRounds.findOne(miniround_id)
	return miniround.number_play==miniround.player_order.length
}

var round_is_over = function(round_id){
	tround = Rounds.findOne(round_id)
	return tround.n_cards==tround.minirounds.length
}

var game_is_over = function(game_id){
	player_count = 0
	this_game = Games.findOne(game_id)
	for(pl in this_game.players){
		player = this_game.players[pl]
		if(player.points>0){
			player_count++
		}
	}
	if(player_count<=1){
		return true
	}
	return false
}

var find_winner = function(miniround_id){
	plays = MiniRounds.findOne(miniround_id).plays
	value = 0
	for(npl in plays){
		if(plays[npl].card.value>value){
			value = plays[npl].card.value
			winner = {id:plays[npl].id,name:plays[npl].name}
		}
	}
	return winner
}

var finalize_miniround = function(miniround_id){
	winner_id = find_winner(miniround_id)
	MiniRounds.update({_id:miniround_id},{$set:{winner:winner_id}})
	round_id = MiniRounds.findOne(miniround_id).round_id
	Rounds.update( { _id: round_id, "minirounds.miniround": miniround_id }, { $set: { "minirounds.$.winner" : winner_id } } )
}

var finalize_round = function(round_id){
	values = new Array()
	preds = Rounds.findOne(round_id).predictions
	for(pr in preds){
		values[preds[pr].id]=preds[pr].pred
	}
	results = Rounds.findOne(round_id).minirounds
	for(r in results){
		values[results[r].winner.id]-=1
	}
	game_id = Rounds.findOne(round_id).game_id
	for(val in values){
		valor = -Math.abs(values[val])
		Games.update({_id:game_id,'players.id':val},{$inc:{'players.$.points':valor}})
	}
	players = Games.findOne(game_id).players
	eliminados = []
	for(pl in players){
		if(players[pl].points<=0){
			eliminados.push(players[pl].id)
		}
	}
	Rounds.update({_id:round_id},{$set:{eliminados:eliminados}})
}

var finalize_game = function(game_id){
	player_count = 0
	this_game = Games.findOne(game_id)
	for(pl in this_game.players){
		player = this_game.players[pl]
		Meteor.users.update({_id:player.id},{$unset:{game_id:"",current_hand:""}})
	}
}

Meteor.methods({
	start_new_game: start_game,
	start_new_round: start_round,
	start_new_miniround: start_miniround,
	send_prediction: function(round_id,prediction){
		if(isNaN(prediction))
			return false;
		prediction = parseInt(prediction)
		var username = Meteor.users.findOne(this.userId).profile.name
		current_round = Rounds.findOne(round_id)
		if(current_round.number_pred==current_round.player_order.length-1){
			cont = 0
			for(npr in current_round.predictions){
				cont=cont+current_round.predictions[npr].pred
			}
			if(cont+prediction==current_round.n_cards){
				return false
			}
		}
		Rounds.update({_id:round_id},{$push:{predictions:{name:username,id:this.userId,pred:prediction}},$inc:{number_pred:1}})
		if(predictions_are_over(round_id)){
			start_miniround(round_id)
		}
		return true
	},
	send_card: function(miniround_id,card){
		cards = Meteor.users.findOne(this.userId).current_hand
		encontrou = false
		for(ncarta in cards){
			carta = cards[ncarta]
			if(carta['na']==card['na'] && carta['nu']==card['nu'])
				encontrou = true				
		}
		username = Meteor.users.findOne(this.userId).profile.name
		MiniRounds.update({_id:miniround_id,"plays.id": this.userId},{$inc:{number_play:1},$set:{"plays.$.card":card}})
		Meteor.users.update({_id:this.userId},{$pull:{current_hand:card}})
		if(miniround_is_over(miniround_id)){
			finalize_miniround(miniround_id)
			round_id = MiniRounds.findOne(miniround_id).round_id
			if(round_is_over(round_id)){
				game_id = MiniRounds.findOne(miniround_id).game_id
				finalize_round(round_id)
				if(game_is_over(game_id)){
					finalize_game(game_id)
					return
				}
				Meteor.setTimeout(function(){
					start_round(game_id)
				},2000)
				
			}else{
				start_miniround(round_id)
			}
		}
	}
});

Meteor.startup(function () {
	Meteor.users.remove({})
	Games.remove({});
	Rounds.remove({});
  });

Meteor.setInterval(function(){
	now = new Date();
	last_p = now.getTime()-1000*60*3;
	Meteor.users.update({'profile.last_use':{$lt:last_p}},{$set:{'profile.idle':true}})
  },1000*60*3);
